<?php

namespace MaelFr\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BlogController extends Controller
{
    public function unsubscribeAction(Request $request)
    {
        $criterias = [
            'mailAddress' => $request->get('address'),
            'token'       => $request->get('token'),
        ];

        $em = $this->getDoctrine()->getEntityManager();
        $mail = $em->getRepository('MaelFrBlogBundle:Mail')->findOneBy($criterias);

        if (!$mail) {
            new NotFoundHttpException('Cette adresse n\'est pas enregistrée en base');
        }

        $em->remove($mail);
        $em->flush();
    }
}
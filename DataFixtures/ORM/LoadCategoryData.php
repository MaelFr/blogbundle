<?php

namespace MaelFr\BlogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use MaelFr\BlogBundle\Entity\Category;

class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $categories = [
            [
                'name' => 'Compétitions',
                'ref' => 'comp',
            ],
            [
                'name' => 'Jeunes',
                'ref' => 'jeunes',
            ],
            [
                'name' => 'Maîtres',
                'ref' => 'master',
            ],
            [
                'name' => 'Vie du club',
                'ref' => 'vdc',
            ],
            [
                'name' => 'ENF',
                'ref' => 'enf',
            ],
        ];

        foreach ($categories as $category) {
            $c = new Category();
            $c->setName($category['name']);

            $manager->persist($c);
            $manager->flush();

            $this->addReference($category['ref'], $c);
        }
    }

    public function getOrder()
    {
        return 2;
    }
}
<?php

namespace MaelFr\BlogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use MaelFr\BlogBundle\Entity\Post;

class LoadPostData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $posts = [
            [
                'title' => 'Un premier article',
                'slug' => 'Un-premier-article',
                'created_at' => new \DateTime('2014-09-01'),
                'last_edit_at' => new \DateTime('2015-01-02'),
                'content' => 'Le texte de ce premier article ne présente aucun intérêt.',
                'categories' => ['comp', 'jeunes'],
                'written_by' => 'user',
                'published' => true,
            ],
            [
                'title' => 'Un second article',
                'slug' => 'Un-second-article',
                'created_at' => new \DateTime('2015-10-08'),
                'content' => 'Le texte de ce second article n\'est pas plus intéressant que celui du premier.',
                'categories' => ['comp', 'master'],
                'written_by' => 'bureau',
                'published' => true,
            ],
            [
                'title' => 'le troisième article',
                'slug' => 'Le-troisième-article',
                'created_at' => new \DateTime('2016-02-29'),
                'content' => 'Cette fois c\'est beaucoup plus intéressant et sérieux. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'categories' => ['vdc'],
                'written_by' => 'user',
                'published' => false,
            ],
        ];

        foreach ($posts as $post) {
            $p = new Post();
            $p->setTitle($post['title']);
//            $p->setSlug($post['slug']);
            $p->setContent($post['content']);
            $p->setCreatedAt($post['created_at']);
            if (isset($post['last_edit_at'])) {$p->setLastEditAt($post['last_edit_at']);}
            foreach ($post['categories'] as $category) {
                $p->addCategory($this->getReference($category));
            }
            $p->setWrittenBy($this->getReference($post['written_by']));
            $p->setPublished($post['published']);
            $p->setImageName('');

            $manager->persist($p);
            $manager->flush();
        }
    }

    public function getOrder()
    {
        return 3;
    }
}
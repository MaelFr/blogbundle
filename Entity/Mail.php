<?php

namespace MaelFr\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Mail
 *
 * @ORM\Table(name="maelfr_blog_mail")
 * @ORM\Entity(repositoryClass="MaelFr\BlogBundle\Repository\MailRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields="mailAddress",
 *     message="Adresse e-mail déjà enregistrée"
 * )
 */
class Mail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Orm\Column(name="mail_address", type="string", length=255, unique=true)
     * @Assert\Email()
     */
    private $mailAddress;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inscription_date", type="datetime")
     */
    private $inscriptionDate;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255)
     */
    private $token;


    /**
     * Get Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mailAddress
     *
     * @param string $mailAddress
     * @return Mail
     */
    public function setMailAddress($mailAddress)
    {
        $this->mailAddress = $mailAddress;

        return $this;
    }

    /**
     * Get mailAddress
     *
     * @return string
     */
    public function getMailAddress()
    {
        return $this->mailAddress;
    }

    /**
     * Set insriptionDate
     *
     * @ORM\PrePersist
     */
    public function setInscriptionDate()
    {
        $this->inscriptionDate = new \DateTime();
    }

    /**
     * Get dateInscription
     *
     * @return \DateTime
     */
    public function getInscriptionDate()
    {
        return $this->inscriptionDate;
    }

    /**
     * Set token
     *
     * @ORM\PrePersist
     */
    public function setToken()
    {
        $this->token = md5($this->mailAddress . md5(date('now')));
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
}
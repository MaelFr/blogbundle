<?php

namespace MaelFr\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Post
 *
 * @ORM\Table(name="maelfr_blog_post")
 * @ORM\Entity(repositoryClass="MaelFr\BlogBundle\Repository\PostRepository")
 */
class Post
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"title"}, updatable=false)
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="last_edit_at", type="datetime", nullable=true)
     */
    private $lastEditAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="published", type="boolean", options={"default":false})
     */
    private $published;

    /**
     * @ORM\ManyToOne(targetEntity="MaelFr\UserBundle\Entity\User")
     */
    private $writtenBy;

    /**
//     * @ORM\OneToMany(targetEntity="MaelFr\UserBundle\Entity\User")
     */
//    private $lastEditBy;

    /**
     * @ORM\ManyToMany(targetEntity="MaelFr\BlogBundle\Entity\Category", inversedBy="posts", cascade={"persist"})
     * @JoinTable(name="maelfr_posts_categories")
     */
    private $categories;


    /**
     * Post constructor.
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

//    /**
//     * Set slug
//     *
//     * @param string $slug
//     * @return Post
//     */
//    public function setSlug($slug)
//    {
//        $this->slug = $slug;
//
//        return $this;
//    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Post
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \Datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \Datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set lastEditAt
     *
     * @param \Datetime $lastEditAt
     */
    public function setLastEditAt($lastEditAt)
    {
        $this->lastEditAt = $lastEditAt;
    }

    /**
     * Get lastEditAt
     *
     * @return \Datetime
     */
    public function getLastEditAt()
    {
        return $this->lastEditAt;
    }

    /**
     * Set published
     * 
     * @param boolean $published
     * @return Post
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return bool
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * Set categories
     *
     * @param array|ArrayCollection $categories
     * @return Post
     */
    public function setCategories($categories)
    {
        foreach ($categories as $category) {
            $this->addCategory($category);
        }

        return $this;
    }

    /**
     * Get categories
     * 
     * @return ArrayCollection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Add category
     *
     * @param Category $category
     * @return Post
     */
    public function addCategory(Category $category)
    {
        if (!$this->categories->contains($category)) {
            $this->categories->add($category);
            $category->addPost($this);
        }

        return $this;
    }

    /**
     * Remove category
     *
     * @param Category $category
     * @return Post
     */
    public function removeCategory(Category $category)
    {
        $category->removePost($this);

        $this->categories->removeElement($category);

        return $this;
    }

    /**
     * Set writtenBy
     *
     * @param mixed $writtenBy
     */
    public function setWrittenBy($writtenBy)
    {
        $this->writtenBy = $writtenBy;
    }

    /**
     * Get writtenBy
     *
     * @return mixed
     */
    public function getWrittenBy()
    {
        return $this->writtenBy;
    }
}

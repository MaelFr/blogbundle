<?php

namespace MaelFr\BlogBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use MaelFr\BlogBundle\Entity\Mail;

class MailEvent extends Event
{
    protected $mail;

    /**
     * MailEvent constructor.
     * @param Mail $mail
     */
    public function __construct(Mail $mail)
    {
        $this->mail = $mail;
    }

    public function getMail()
    {
        return $this->mail;
    }
}
<?php

namespace MaelFr\BlogBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use MaelFr\BlogBundle\Entity\Mail;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MailRegistrationListener
{
    protected $container;
    protected $emailFrom;

    public function __construct(ContainerInterface $container, $emailFrom)
    {
        $this->container = $container;
        $this->emailFrom = $emailFrom;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        /** @var Mail $mail */
        $mail = $args->getEntity();

        if (is_a($mail, Mail::class)) {
            $message = \Swift_Message::newInstance()
                ->setSubject('Inscription à la liste de diffusion')
                ->setFrom($this->emailFrom)
                ->setTo($mail->getMailAddress())
                ->setBody($this->container->get('twig')->render(
                    'MaelFrBlogBundle:Mail:registration.html.twig',
                    [
                        'mail'  => $mail,
                    ]
                ))
            ;
            $this->container->get('mailer')->send($message);
        }
    }
}
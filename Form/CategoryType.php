<?php

namespace MaelFr\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom',
            ])
            ->add(
                'description',
                TextareaType::class,
                [
                    'label' => 'Description',
                    'attr'  => [
                        'rows' => '5',
                    ],
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'MaelFr\BlogBundle\Entity\Category',
            ]
        );
    }

    public function getName()
    {
        return 'maelfr_blog_category_type';
    }
}

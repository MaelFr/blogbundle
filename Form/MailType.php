<?php

namespace MaelFr\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mailAddress', EmailType::class, [
                'label'  => 'Adresse e-mail',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'MaelFr\BlogBundle\Entity\Mail',
        ]);
    }

    public function getName()
    {
        return 'maelfr_blog_mail_type';
    }

    public function getBlockPrefix()
    {
        return $this->getName();
    }
}
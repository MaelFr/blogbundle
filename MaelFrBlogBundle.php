<?php

namespace MaelFr\BlogBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use MaelFr\BlogBundle\DependencyInjection\MaelFrBlogExtension;

class MaelFrBlogBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new MaelFrBlogExtension();
    }
}

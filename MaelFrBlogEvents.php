<?php

namespace MaelFr\BlogBundle;

final class MaelFrBlogEvents
{
    const BLOG_MAIL_CREATED = 'maelfr_blog.mail.created';
    const BLOG_MAIL_DELETED = 'maelfr_blog.mail.deleted';
}
<?php

namespace MaelFr\BlogBundle\Manager;

use Doctrine\ORM\EntityManager;

abstract class BaseManager
{
    /** @var EntityManager $em */
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function saveOne($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    public function removeOne($entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }
}
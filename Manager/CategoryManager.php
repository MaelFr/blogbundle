<?php

namespace MaelFr\BlogBundle\Manager;

use Doctrine\ORM\EntityManager;
use MaelFr\BlogBundle\Repository\CategoryRepository;

class CategoryManager extends BaseManager
{
    /** @var CategoryRepository $repo */
    protected $repo;

    public function __construct(EntityManager $em, CategoryRepository $repo)
    {
        $this->repo = $repo;
        parent::__construct($em);
    }

    public function getForSidebar()
    {
        return $this->repo->getForSidebar();
    }
}
<?php

namespace MaelFr\BlogBundle\Manager;

use Doctrine\ORM\EntityManager;
use MaelFr\BlogBundle\Repository\MailRepository;

class MailManager extends BaseManager
{
    /** @var MailRepository $repo */
    protected $repo;

    public function __construct(EntityManager $em, MailRepository $repo)
    {
        $this->repo = $repo;
        parent::__construct($em);
    }

    /**
     * @param $mail
     * @param $token
     *
     * @return null|object
     */
    public function findOneByMailAndToken($mail, $token)
    {
        $criteria = [
            'mailAddress' => $mail,
            'token'       => $token,
        ];

        return $this->repo->findOneBy($criteria);
    }
}
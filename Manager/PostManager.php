<?php

namespace MaelFr\BlogBundle\Manager;

use Doctrine\ORM\EntityManager;
use MaelFr\BlogBundle\Repository\PostRepository;

class PostManager extends BaseManager
{
    /** @var PostRepository $repo */
    protected $repo;

    public function __construct(EntityManager $em, PostRepository $repo)
    {
        $this->repo = $repo;
        parent::__construct($em);
    }

    public function getAll()
    {
        $this->repo->getAll();
    }

    /**
     * @param integer $number
     * @return array
     */
    public function getForIndex($number = null) {
        return $this->repo->getForIndex($number);
    }

    public function getNbPosts()
    {
        return $this->repo->getNbPosts();
    }
}
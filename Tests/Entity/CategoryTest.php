<?php

namespace MaelFr\BlogBundle\Tests\Entity;

use MaelFr\BlogBundle\Entity\Category;

/**
 * @covers Category
 */
class CategoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var Category $category */
    protected $category;

    public function setUp()
    {
        $c = new Category();
        $c->setName('Test');
        $c->setDescription('Description en plusieurs mots');
        $this->category = $c;
    }

    public function testGetName()
    {
        $this->assertEquals('Test', $this->category->getName());
    }

    public function testGetDescription()
    {
        $this->assertEquals('Description en plusieurs mots', $this->category->getDescription());
    }

//    public function testGetPosts()
//    {
//        $this->assertEquals(ArrayCollection::class, $this->category->getPosts());
//    }
}
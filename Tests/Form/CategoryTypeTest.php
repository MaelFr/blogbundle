<?php

namespace MaelFr\BlogBundle\Tests\Form;

use Symfony\Component\Form\Test\TypeTestCase;
use MaelFr\BlogBundle\Entity\Category;
use MaelFr\BlogBundle\Form\CategoryType;

class CategoryTypeTest extends TypeTestCase
{
    public function testSubmitValidData()
    {
        $formData = [
            'name' => 'Test',
            'description' => 'Un texte de test.',
        ];

        $form = $this->factory->create(CategoryType::class);

        $category = new Category;
        $category->setName($formData['name']);
        $category->setDescription($formData['description']);

        // submit the data to the form directly
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($category, $form->getData());

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }

//        $this->assertEquals('maelfr_blog_category_type', $form->getConfig()->get);
    }
}
<?php

namespace MaelFr\BlogBundle\Twig;

class BlogExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    private $params;

    /**
     * BlogExtension constructor.
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    public function getGlobals()
    {
        return [
            'indexPostLength' => $this->params['index']['post_length'],
        ];
    }

    public function getName()
    {
        return 'blog_extension';
    }
}